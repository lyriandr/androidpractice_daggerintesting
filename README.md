## Application

Application for practice in Dagger-Android

## Technical Requirements
- Use dagger-android library and corresponding AndroidInjection.inject(this) call to inject Android SKD dependable classes affected by lifecycle.
- Inject objects graph in test class of androidTest package

## Technology stack

- Language - Java
- Dagger2 [https://github.com/google/dagger](https://github.com/google/dagger)
- Dagger-Android library