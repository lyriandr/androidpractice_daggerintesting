package ru.lyrian.androidpractice_daggerintesting;

import android.util.Log;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import ru.lyrian.androidpractice_daggerintesting.di.app.TestApp;
import ru.lyrian.androidpractice_daggerintesting.di.component.DaggerIAppComponent;
import ru.lyrian.androidpractice_daggerintesting.model.StringStorage;
import ru.lyrian.androidpractice_daggerintesting.presentation.MainActivity;
import ru.lyrian.androidpractice_daggerintesting.utility.Util;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

public class MainActivityTest {
    StringStorage stringStorage;

    @Rule
    public ActivityScenarioRule<MainActivity> activityScenarioRule = new ActivityScenarioRule<>(MainActivity.class);

    @Before
    public void setUp() {
        TestApp testApp = ApplicationProvider.getApplicationContext();
        this.stringStorage = testApp.getIAppComponent().stringStorage();
        Log.d(Util.LOG_TAG, "MainActivityTest: " + this.stringStorage);
        Log.d(Util.LOG_TAG, "MainActivityTest: " + this.stringStorage.getTextForSubstitution());
    }

    @Test
    public void viewsInitialValue() {
        onView(withId(R.id.tvChangeableText)).check(matches(withText(R.string.tv_initial_text)));
        onView(withId(R.id.btChangeText)).check(matches(withText(R.string.bt_change_text_hint)));
    }

    @Test
    public void viewValueChanged() {
        onView(withId(R.id.btChangeText)).perform(click());
        onView(withId(R.id.tvChangeableText)).check(matches(withText(this.stringStorage.getTextForSubstitution())));
    }
}
