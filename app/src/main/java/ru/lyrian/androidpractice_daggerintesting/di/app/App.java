package ru.lyrian.androidpractice_daggerintesting.di.app;

import android.app.Application;


import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import ru.lyrian.androidpractice_daggerintesting.di.component.DaggerIAppComponent;
import ru.lyrian.androidpractice_daggerintesting.di.component.IAppComponent;

public class App extends DaggerApplication {
    private IAppComponent iAppComponent;

    public IAppComponent getIAppComponent() {
        return iAppComponent;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        this.iAppComponent = DaggerIAppComponent.create();
        return this.iAppComponent;
    }
}
