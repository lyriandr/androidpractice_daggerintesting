package ru.lyrian.androidpractice_daggerintesting.di.component;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import ru.lyrian.androidpractice_daggerintesting.databinding.ActivityMainBinding;
import ru.lyrian.androidpractice_daggerintesting.di.app.App;
import ru.lyrian.androidpractice_daggerintesting.di.modules.ActivityBuildersModule;
import ru.lyrian.androidpractice_daggerintesting.di.modules.MvpModule;
import ru.lyrian.androidpractice_daggerintesting.model.StringStorage;
import ru.lyrian.androidpractice_daggerintesting.presentation.MainActivity;

@Singleton
@Component(modules = {
        AndroidInjectionModule.class,
        MvpModule.class,
        ActivityBuildersModule.class})
public interface IAppComponent extends AndroidInjector<App> {
    StringStorage stringStorage();
}
