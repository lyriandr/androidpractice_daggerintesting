package ru.lyrian.androidpractice_daggerintesting.di.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import ru.lyrian.androidpractice_daggerintesting.presentation.MainActivity;

@Module
public interface ActivityBuildersModule {
    @ContributesAndroidInjector
    MainActivity contributeMainActivity();
}
