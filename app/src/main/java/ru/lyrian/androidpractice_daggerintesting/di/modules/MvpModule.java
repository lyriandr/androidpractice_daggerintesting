package ru.lyrian.androidpractice_daggerintesting.di.modules;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import ru.lyrian.androidpractice_daggerintesting.model.IStringContract;
import ru.lyrian.androidpractice_daggerintesting.model.StringStorage;
import ru.lyrian.androidpractice_daggerintesting.presentation.IMainContract;
import ru.lyrian.androidpractice_daggerintesting.presentation.MainPresenter;

@Module
public interface MvpModule {
    @Binds
    @Singleton
    IStringContract.IStringStorage provideStringStorage(StringStorage stringStorage);

    @Binds
    @Singleton
    IMainContract.IPresenter provideMainPresenter(MainPresenter mainPresenter);
}

