package ru.lyrian.androidpractice_daggerintesting.model;

import io.reactivex.Observable;
import io.reactivex.Single;

public interface IStringContract {
    interface IStringStorage {
        Single<String> getStringObservable();
    }

    interface IStringObserver {
        void publishString(String s);
    }
}
