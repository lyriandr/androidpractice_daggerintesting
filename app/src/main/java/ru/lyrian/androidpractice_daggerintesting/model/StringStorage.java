package ru.lyrian.androidpractice_daggerintesting.model;

import android.util.Log;

import java.time.LocalTime;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import ru.lyrian.androidpractice_daggerintesting.utility.Util;

@Singleton
public class StringStorage implements IStringContract.IStringStorage {
    private final String textForSubstitution = LocalTime.now().toString();

    @Inject
    public StringStorage() {
        Log.d(Util.LOG_TAG, "StringStorage: " + this);
        Log.d(Util.LOG_TAG, "StringStorage: " + this.textForSubstitution);
    }

    public String getTextForSubstitution() {
        return this.textForSubstitution;
    }

    @Override
    public Single<String> getStringObservable() {
        return Single
                .just(this.textForSubstitution)
                .subscribeOn(Schedulers.computation());
    }
}
