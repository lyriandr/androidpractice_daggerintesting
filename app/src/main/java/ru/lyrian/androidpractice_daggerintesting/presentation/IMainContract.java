package ru.lyrian.androidpractice_daggerintesting.presentation;

public interface IMainContract {
    interface IPresenter {
        void attach(IView iView);

        void detach();

        void fetchString();
    }

    interface IView {
        void setTextViewValue(String value);
    }
}
