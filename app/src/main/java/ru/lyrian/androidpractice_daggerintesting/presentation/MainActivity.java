package ru.lyrian.androidpractice_daggerintesting.presentation;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import ru.lyrian.androidpractice_daggerintesting.databinding.ActivityMainBinding;
import ru.lyrian.androidpractice_daggerintesting.di.app.App;
import ru.lyrian.androidpractice_daggerintesting.di.component.IAppComponent;

public class MainActivity extends DaggerAppCompatActivity implements IMainContract.IView {
    private ActivityMainBinding activityMainBinding;

    @Inject
    IMainContract.IPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = this.activityMainBinding.getRoot();
        setContentView(view);
        this.activityMainBinding.btChangeText.setOnClickListener(v -> this.presenter.fetchString());
    }

    @Override
    protected void onStart() {
        super.onStart();

        this.presenter.attach(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        this.presenter.detach();
    }

    @Override
    public void setTextViewValue(String text) {
        this.activityMainBinding.tvChangeableText.setText(text);
    }
}