package ru.lyrian.androidpractice_daggerintesting.presentation;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;

import javax.inject.Inject;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import ru.lyrian.androidpractice_daggerintesting.model.IStringContract;

public class MainPresenter implements IMainContract.IPresenter, IStringContract.IStringObserver {
    private WeakReference<IMainContract.IView> view;
    private IStringContract.IStringStorage stringStorage;
    private Disposable stringDisposable;

    @Inject
    public MainPresenter(IStringContract.IStringStorage stringStorage) {
        this.stringStorage = stringStorage;
    }

    @Override
    public void attach(IMainContract.IView iView) {
        this.view = new WeakReference<>(iView);
    }

    @Override
    public void detach() {
        this.view = null;
    }

    @Override
    public void fetchString() {
        this.stringStorage
                .getStringObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<String>() {
                    @Override
                    public void onSubscribe(@io.reactivex.annotations.NonNull Disposable d) {
                        stringDisposable = d;
                    }

                    @Override
                    public void onSuccess(@io.reactivex.annotations.NonNull String s) {
                        publishString(s);
                        stringDisposable.dispose();
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void publishString(String s) {
        if (this.view != null && this.view.get() != null) {
            this.view.get().setTextViewValue(s);
        }
    }
}
